package com.faisal.soalcolega.service;

import com.faisal.soalcolega.model.entity.Kota;
import com.faisal.soalcolega.model.repository.KotaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class KotaService {

    @Autowired
    private KotaRepository kotaRepository;

    public Iterable<Kota> getAllKota(){
        return kotaRepository.findAll();
    }

    public Kota getKotaById(String id){
        Optional<Kota> kota = kotaRepository.findById(id);
        if (!kota.isPresent()){
            return null;
        }
        return kotaRepository.findById(id).get();
    }

    public Kota addkota(Kota kota){
        return kotaRepository.save(kota);
    }

    public void deleteKota(String id){
        kotaRepository.deleteById(id);
    }

    public long countRepo(){
        return kotaRepository.count();
    }

}
