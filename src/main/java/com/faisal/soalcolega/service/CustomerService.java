package com.faisal.soalcolega.service;

import com.faisal.soalcolega.dto.CustomerDto;
import com.faisal.soalcolega.model.entity.Customer;
import com.faisal.soalcolega.model.entity.Kota;
import com.faisal.soalcolega.model.repository.CustomerHelperRepository;
import com.faisal.soalcolega.model.repository.CustomerRepository;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import javax.transaction.TransactionScoped;
import javax.transaction.Transactional;
import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
@Transactional
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerHelperRepository customerHelperRepository;

    public Iterable<CustomerDto> getAll(){
        return customerHelperRepository.findAll();
    }

    public Customer addCustomer(Customer customer){
        return customerRepository.save(customer);
    }

    public void deleteCustomer(String id){
        customerRepository.deleteById(id);
    }

    public Customer getCustomerById(String id){
        Optional<Customer> customer = customerRepository.findById(id);
        if (!customer.isPresent()){
            return null;
        }
        return customerRepository.findById(id).get();
    }

    public long countRepo(){
        return customerRepository.count();
    }

    public String exportPdf() throws FileNotFoundException, JRException {
        String path = "C:\\Users\\dukcapil\\Downloads";
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy-HH-mm-ss");
        String formatTanggal = localDateTime.format(format);
        List<CustomerDto> customerDtos = (List<CustomerDto>) customerHelperRepository.findAll();
        File file = ResourceUtils.getFile("classpath:customer.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(customerDtos);
        Map<String,Object> param = new HashMap<>();
        param.put("createdBy","faisal");
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,param,dataSource);
        JasperExportManager.exportReportToPdfFile(jasperPrint,path + "\\customer"+ formatTanggal.toString()  + ".pdf");
        return "report generated in path" + path;

    }

}
