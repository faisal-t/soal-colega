package com.faisal.soalcolega.model.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "customer")
public class Customer {

    @Id
    @NotEmpty()
    @Column(name = "CUST_ID",length = 7)
    private String cust_id;

    @NotEmpty(message = "Nama Tidak Boleh Kosong")
    @Column(name = "NAMA",length = 50)
    private String nama;

    @NotEmpty
    @Column(name = "ALAMAT",length = 255)
    private String alamat;


    @NotEmpty
    @Column(name = "ID_KOTA",length = 3)
    private String id_kota;


    @Column(name = "PENDAPATAN")
    private Double pendapatan;


    public Customer() {
    }

    public Customer(String cust_id, String nama, String alamat, String id_kota, Double pendapatan) {
        this.cust_id = cust_id;
        this.nama = nama;
        this.alamat = alamat;
        this.id_kota = id_kota;
        this.pendapatan = pendapatan;
    }

    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getId_kota() {
        return id_kota;
    }

    public void setId_kota(String id_kota) {
        this.id_kota = id_kota;
    }

    public Double getPendapatan() {
        return pendapatan;
    }

    public void setPendapatan(Double pendapatan) {
        this.pendapatan = pendapatan;
    }
}
