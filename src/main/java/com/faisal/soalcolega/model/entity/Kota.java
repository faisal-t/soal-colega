package com.faisal.soalcolega.model.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "kota")
public class Kota {

    @Id
    @NotEmpty
    @Column(name = "ID_KOTA",length = 3)
    private String id_kota;

    @Column(name = "NAMA",length = 32)
    @NotEmpty(message = "Nama Tidak Boleh Kosong")
    private String nama;

    public Kota() {
    }

    public Kota(String id_kota, String nama) {
        this.id_kota = id_kota;
        this.nama = nama;
    }

    public String getId_kota() {
        return id_kota;
    }

    public void setId_kota(String id_kota) {
        this.id_kota = id_kota;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
