package com.faisal.soalcolega.model.repository;

import com.faisal.soalcolega.dto.CustomerDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerHelperRepository extends JpaRepository<CustomerDto,String> {
}
