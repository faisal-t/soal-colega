package com.faisal.soalcolega.model.repository;

import com.faisal.soalcolega.model.entity.Kota;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KotaRepository extends JpaRepository<Kota,String> {
}
