package com.faisal.soalcolega;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoalcolegaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoalcolegaApplication.class, args);
	}

}
