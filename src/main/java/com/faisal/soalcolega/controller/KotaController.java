package com.faisal.soalcolega.controller;

import com.faisal.soalcolega.model.entity.Kota;
import com.faisal.soalcolega.service.KotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/kota")
public class KotaController {

    @Autowired
    private KotaService kotaService;

    @GetMapping
    public String getAll(Model model){
        long count = kotaService.countRepo();
        int id = String.valueOf(count).length();
        String newId;
        if (id == 1){
            newId = "00" + String.valueOf(count + 1);
        }
        else if (id == 2){
            newId = "0" + String.valueOf(count + 1);
        }
        else{
            newId = String.valueOf(count + 1);
        }

        model.addAttribute("newid",newId);
        model.addAttribute("kota",kotaService.getAllKota());
        model.addAttribute("kotainput",new Kota());
        return "kota";
    }

    @PostMapping
    public String addKota(Kota kota){
        kotaService.addkota(kota);
        return "redirect:/kota";
    }

    @GetMapping("/delete/{id}")
    public String deleteKota(@PathVariable String id){
        kotaService.deleteKota(id);
        return "redirect:/kota";
    }




}
