package com.faisal.soalcolega.controller;

import com.faisal.soalcolega.dto.CustomerDto;
import com.faisal.soalcolega.model.entity.Customer;
import com.faisal.soalcolega.service.CustomerService;
import com.faisal.soalcolega.service.KotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customer")
public class CustomerController2 {
    @Autowired
    private CustomerService customerService;

    @Autowired
    private KotaService kotaService;

    @GetMapping("/detail")
    public Iterable<CustomerDto> customer(){
        return customerService.getAll();
    }

    @GetMapping("/{id}")
    public Customer getOneCustomer(@PathVariable String id){
        return customerService.getCustomerById(id);
    }


}
