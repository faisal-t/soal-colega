package com.faisal.soalcolega.controller;

import com.faisal.soalcolega.model.entity.Customer;
import com.faisal.soalcolega.model.entity.Kota;
import com.faisal.soalcolega.service.KotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/kota")
public class KotaRestController {

    @Autowired
    private KotaService kotaService;

    @GetMapping("/{id}")
    public Kota getoneKota(@PathVariable String id){
        return kotaService.getKotaById(id);
    }

}
