package com.faisal.soalcolega.controller;

import com.faisal.soalcolega.dto.CustomerDto;
import com.faisal.soalcolega.model.entity.Customer;

import com.faisal.soalcolega.service.CustomerService;
import com.faisal.soalcolega.service.KotaService;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.FileNotFoundException;

@Controller
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private KotaService kotaService;

    @GetMapping()
    public String customer(Model model){

        long count = customerService.countRepo();
        int id = String.valueOf(count).length();
        String newId;
        if (id == 1){
            newId = "00000" + String.valueOf(count + 1);
        }
        else if (id == 2){
            newId = "0000" + String.valueOf(count + 1);
        }
        else if (id == 3){
            newId = "000" + String.valueOf(count + 1);
        }
        else if (id == 4){
            newId = "00" + String.valueOf(count + 1);
        }
        else if (id == 5){
            newId = "0" + String.valueOf(count + 1);
        }
        else{
            newId = String.valueOf(count + 1);
        }
        model.addAttribute("newid",newId);
        model.addAttribute("customer",customerService.getAll());
        model.addAttribute("kota",kotaService.getAllKota());
        model.addAttribute("customerinput", new Customer());
        return "customer";
    }


    @PostMapping
    public String PostCustomer(@Valid Customer customer, BindingResult result){
        if (result.hasErrors()) {
            return "redirect:/customer";
        }

        customerService.addCustomer(customer);
        return "redirect:/customer";
    }



    @GetMapping("/delete/{id}")
    public String DeleteCustomer(@PathVariable String id){
        customerService.deleteCustomer(id);
        return "redirect:/customer";
    }

    @GetMapping("/pdf")
    public String getPdf() throws JRException, FileNotFoundException {
        customerService.exportPdf();
        return "redirect:/customer";
    }




}
