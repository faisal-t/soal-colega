package com.faisal.soalcolega.dto;


import com.faisal.soalcolega.model.entity.Kota;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "customer")
public class CustomerDto {

    @Id
    @NotEmpty
    @Column(name = "CUST_ID",length = 6)
    private String cust_id;

    @NotEmpty
    @Column(name = "NAMA",length = 50)
    private String nama;

    @NotEmpty
    @Column(name = "ALAMAT",length = 255)
    private String alamat;

    @Column(name = "PENDAPATAN")
    private Double pendapatan;

    @NotEmpty
    @ManyToOne
    @JoinColumn(name="ID_KOTA")
    private Kota id_kota;

    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public Double getPendapatan() {
        return pendapatan;
    }

    public void setPendapatan(Double pendapatan) {
        this.pendapatan = pendapatan;
    }

    public Kota getId_kota() {
        return id_kota;
    }

    public void setId_kota(Kota id_kota) {
        this.id_kota = id_kota;
    }
}
